package com.ocp8.leokenzley.lambdas;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import static java.util.Arrays.asList;


public class LambdaTeste {

public static void main(String[] args) {
	SuperHeroi superHeroi = new SuperHeroi("Goku", 50);
	System.out.println("************************ Predicate **********************");
	// Uso da lambda do tipo Predicate, pois possui um m�todo boolean	
	Predicate<SuperHeroi> shp2 = (SuperHeroi sh)->{ return sh.getNome().endsWith("gu");};
	System.out.println(shp2.test(superHeroi));
	
	System.out.println("************************ Consumer **********************");
	imprimeTudoConsumer(asList("A", "B", "C", "D"), (String s) -> {System.out.println(s);}); 
	
	
	System.out.println("************************ Function **********************");
}	


	static <T> void imprimeTudoConsumer(List<T> lista, Consumer<T> c) {
		if(lista.size() > 0) {
			for(T t : lista) {
				c.accept(t);
			}
		}
	}
	
	
}
