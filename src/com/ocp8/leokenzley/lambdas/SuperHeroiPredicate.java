package com.ocp8.leokenzley.lambdas;

@FunctionalInterface
public interface SuperHeroiPredicate {
	boolean test(SuperHeroi superHeroi);
}
