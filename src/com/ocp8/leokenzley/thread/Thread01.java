package com.ocp8.leokenzley.thread;

import static java.util.Arrays.asList;

import java.util.List;


class ThreadClass extends Thread{

	public void run() {
		List<String> nomes = asList("Leo", "Kenzley", "Beserra", "de", "Oliveira");
		for(String nome: nomes) {
			System.out.println("XXXXX");
			System.out.println(nome);
			System.out.println("XXXXX");
		}
	}
	
}


public class Thread01{
	public static void main(String[] args) {
		Thread t = new Thread(new ThreadClass());
		t.start();
	}
}